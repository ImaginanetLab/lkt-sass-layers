# Installation

In your package.json:

```
dependencies: {
...
    "lkt-sass-layers": "git+https://gitlab.com/Lekrat/lkt-sass-layers.git#1.0.0",
...
}
```

npm update

# Usage

## NPM

```
@import "~lkt-sass-shapes/src/lkt-sass-shapes.scss";
```

## Gulp

In your main.scss:

```
@import "./../path/to/../node_modules/lkt-sass-layers/src/lkt-sass-layers";
```

# Configure

After import in your project, you can add layers:

```
$add: lkt-layer-add(default, 1);
$add: lkt-layer-add(modal, 10);
$add: lkt-layer-add(sample, 50);
$add: lkt-layer-add(something, 100);
```

# Api

Access layers through mixin:

```
.sample
{
    @include lkt-layer(sample);
}
```

Access layers through function:

```
.sample
{
    z-index: lkt-layer-get(sample);
}
```